import pandas as pd
import numpy as np

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()
    titles = ['Mr.', 'Mrs.', 'Miss.']
    results = []

    for title in titles:
        title_data = df[df['Name'].str.contains(title)]
        median_age = title_data['Age'].median()
        missing_values = title_data['Age'].isnull().sum()
        
        rounded_median_age = round(median_age)
        results.append((title, missing_values, rounded_median_age))

    return results
